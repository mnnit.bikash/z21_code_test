import csv
import datetime


school_details = []


def load_school_dictionary():
    """
    Parse the school file and store data in dictionary school_dictionary
    :return: None
    """

    with open("school_data.csv") as fh:
        csv_reader = csv.DictReader(fh)
        for row in csv_reader:
            school_details.append([row['SCHNAM05'], row['LSTATE05'], row['LCITY05']])


def search_schools(search_string):
    '''
    Search school and print top three results
    :param search_string: string to be serched
    :return: None
    '''
    if not bool(school_details):
        load_school_dictionary()
    start_time = datetime.datetime.now()

    search_string_org = search_string
    search_string = search_string.lower()

    result_1 = [0, None]
    result_2 = [0, None]
    result_3 = [0, None]

    search_string_set = set(search_string.split(' '))
    if 'school' in search_string_set:
        search_string_set.remove('school')

    for sc in school_details:
        sc_set = set(sc[0].lower().split(' '))
        common_len = len(search_string_set & sc_set)
        if sc[0].lower() == search_string:
            common_len = common_len + 1  # Adding preference if it is exact match
        if common_len > 0:
            if common_len > result_1[1]:
                result_3 = result_2
                result_2 = result_1
                result_1 = [common_len, sc]
            elif common_len > result_2[1]:
                result_3 = result_2
                result_2 = [common_len, sc]
            elif common_len > result_3[1]:
                result_3 = [common_len, sc]
    end_time = datetime.datetime.now()
    delta = end_time - start_time
    time_elapsed = delta.seconds + delta.microseconds / 1000000.0

    print('Results for "{}" (search took: {})'.format(search_string_org, time_elapsed))
    if result_1[1] is not None:
        print("1. {}\n{}, {}".format(result_1[1][0], result_1[1][2], result_1[1][1]))
    if result_2[1] is not None:
        print("1. {}\n{}, {}".format(result_2[1][0], result_2[1][2], result_2[1][1]))
    if result_3[1] is not None:
        print("1. {}\n{}, {}".format(result_3[1][0], result_3[1][2], result_3[1][1]))


load_school_dictionary()  # loading file in advance
