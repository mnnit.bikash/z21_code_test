import csv


def print_counts():
    """
    Read the csv file and print summary
    :return: None
    """
    school_count = 0
    schools_per_state = {}
    schools_per_city = {}
    schools_per_metro_centric = {}

    with open('school_data.csv') as fh:
        csv_reader = csv.DictReader(fh)
        for row in csv_reader:
            school_count = school_count + 1
            schools_per_state[row['LSTATE05']] = schools_per_state.setdefault(row['LSTATE05'], 0) + 1
            schools_per_city[row['LCITY05']] = schools_per_city.setdefault(row['LCITY05'], 0) + 1
            schools_per_metro_centric[row['MLOCALE']] = schools_per_metro_centric.setdefault(row['MLOCALE'], 0) + 1

    city_with_most_schools = []
    for key, val in schools_per_city.iteritems():
        if not city_with_most_schools or val == city_with_most_schools[0][1]:
            # There can be more than one city with highest number of schools
            city_with_most_schools.append([key, val])
        elif val > city_with_most_schools[0][1]:
            city_with_most_schools = [[key, val]]

    print("Total Schools: {}".format(school_count))
    print("Schools by State:")
    for key, val in schools_per_state.iteritems():
        print("{}: {}".format(key, val))
    print("Schools by Metro-centric locale:")
    for key, val in schools_per_metro_centric.iteritems():
        print("{}: {}".format(key, val))
    print("City with most schools: {} ({} schools)".format(','.join([x[0] for x in city_with_most_schools]),
                                                           ','.join([str(x[1]) for x in city_with_most_schools])))
    print("Unique cities with at least one school: {}".format(len(schools_per_city)))
